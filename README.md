# Two different ways of using NETSCITY to map and analyse world networks of scientific collaboration

## Deux différentes manières d’utiliser NETSCITY pour cartographier et analyser les réseaux mondiaux de collaboration scientifique

ENG: This presentation, designed as a tutorial, presents two different ways of using the NETSCITY web application to map and analyse collaborative networks on a world scale. After a summary of how the application works in general, I propose two step-by-step examples of use that can be reproduced and carried out live during the presentation:
- Extracting data from the Web of Science - loading it into NETSCITY - exploring the graphical results generated automatically in the application and exporting a map of the distribution of scientific production in vector format.
- Build up a corpus from the source of your choice - load it into NETSCITY - export the table of addresses obtained after geocoding and transform the result obtained into a graph-type object in R in order to carry out a network analysis (e.g. calculate the density of the graph).

FR : Cette présentation pensée comme un tutoriel présente deux différentes manières d’utiliser l’application web NETSCITY pour cartographier et analyser des réseaux de collaboration à l’échelle mondiale. Après un résumé du fonctionnement général de l’application, nous proposons deux exemples d’utilisation pas à pas qui pourront être reproduits et réalisés en direct au cours de la présentation:
- Extraire des données du Web of Science – les charger dans NETSCITY – explorer les résultats graphiques générés automatiquement dans l’application et exporter une carte de répartition de la production scientifique au format vectoriel
- Constituer un corpus à partir de la source de votre choix – le charger dans NETSCITY – exporter la table d’adresses obtenue après géocodage et transformer le résultat obtenu en objet de type graphe dans R afin de réaliser une analyse de réseaux (e.g. calcul de la densité du graphe).

