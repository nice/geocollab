
library(tidyverse)
library(igraph)
library(ggraph)

map(c("data", "plots"), dir.create) # create two new files in the repo

# upload the address table

a <- read_csv2("data/655cbe739f4be.csv")

# diplay the columns' names

colnames(a)

'[1] "id"            "year"          "address"       "city"          "agglo"         "province"      "iso"          
 [8] "country"       "subregion"     "region"        "continent"     "latitude"      "longitude"     "idcomposite"  
[15] "confidence"    "manual_update"'

# proprocess the data to get an edgelist at the urban area level

a <- a %>%
  distinct(id, idcomposite, agglo, province, country, continent) %>% # remove duplicates
  rename(from = id, to = idcomposite) %>% # rename the "origin" and "destination" columns
  mutate(agglo = ifelse(agglo %in% "LANNION", "ROSCOFF", agglo)) # rename the Lannion agglomeration into "Roscoff"

b <- graph_from_data_frame(a, directed = F) # get a bipartite graph (igraph object)

V(b)$type <- ifelse(str_detect(V(b)$name, "WOS"), 0, 1) # assign the nodes to their bipartite type

g <- bipartite_projection(b, multiplicity = TRUE, which = "true") # project the graph into a one-mode network

g # check for the network's characteristics (number of nodes, of edges etc.)

'IGRAPH 258eba1 UNW- 160 2039 -- 
+ attr: name (v/c), label (v/c), country (v/c), continent (v/c), weight (e/n)
+ edges from 258eba1 (vertex names):
 [1] AAPOLY6753--AAPOLY24610 AAPOLY6753--AAPOLY24041 AAPOLY6753--AD12208     AAPOLY6753--AD4381     
 [5] AAPOLY6753--AD8508      AAPOLY6753--AAPOLY9822  AAPOLY6753--AAPOLY5882  AAPOLY6753--AD10833    
 [9] AAPOLY6753--AD11968     AAPOLY6753--AD5683      AAPOLY6753--AAPOLY28843 AAPOLY6753--AD16155    
[13] AAPOLY6753--AD518       AAPOLY6753--AD1033      AAPOLY6753--AD1983      AAPOLY6753--AAPOLY4650 
[17] AAPOLY6753--AD3612      AAPOLY6753--AD3567      AAPOLY6753--AAPOLY8819  AAPOLY6753--AD4506     
[21] AAPOLY6753--AD6384      AAPOLY6753--AD6748      AAPOLY6753--AD9100      AAPOLY6753--AD12617    
[25] AAPOLY6753--AAPOLY28554 AAPOLY6753--AD14825     AAPOLY6753--AD15081     AAPOLY6753--AD15377    
[29] AAPOLY6753--AD16121     AAPOLY6753--AD14548     AAPOLY6753--AD16209     AAPOLY6753--AD16654    
+ ... omitted several edges'

graph.density(g) # compute the graph density

"[1] 0.1602987"

# add the nodes' attributes (name, country, continent)

V(g)$label <- a$agglo[match(V(g)$name, a$to)]
V(g)$country <- a$country[match(V(g)$name, a$to)]
V(g)$continent <- a$continent[match(V(g)$name, a$to)]

# save the spatial layout of the graph according to the fruchterman reingold algorithm

l <- layout_with_fr(g)

# plot the graph

g %>%
  ggraph(layout = l) + 
  geom_edge_link(aes(filter = E(g)$weight >= 2), edge_colour = "grey") +
  geom_node_point(aes(size = degree(g), fill = V(g)$continent), shape = 22, stroke = 0.2) + 
  geom_node_text(aes(label = str_to_title(label), 
                     fontface = ifelse(degree(g) >= 40, "bold", "plain")), 
                 size = ifelse(degree(g) >= 40, 3, 2), 
                 colour = "black", 
                 repel = T, show.legend = F) + 
  theme_void() +
  labs(size = "Number of distinct partners (unweighted degree)") +
  theme(legend.title = element_text(size = 6),
        legend.text = element_text(size = 5),
        legend.position = "bottom")

# save the plot in different formats (vector file and image file)

ggsave("plots/graph_oban_roscoff_cooperation_network_wos.pdf")
ggsave("plots/graph_oban_roscoff_cooperation_network_wos.svg", dpi = 300)
ggsave("plots/graph_oban_roscoff_cooperation_network_wos.png", width = 13.25, height = 9)
